package nk.omsu
import java.io._
import java.nio.ByteBuffer
import java.util
import java.util.regex.{Matcher, Pattern}
import javax.swing.JFrame

import org.jfree.chart.{ChartFactory, ChartPanel, JFreeChart}
import org.jfree.chart.plot.PlotOrientation
import org.jfree.data.xy.{XYDataset, XYSeries, XYSeriesCollection}

import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.util.Random;
/**
  * Created by nk16 on 06.09.16.
  */

object Main {

  var Hk = new Array[Double](5)
  def log2(value:Double):Double={
  val d =   Math.log(value) / Math.log(2)
    d
  }
  def graf(): Unit ={
    var series:XYSeries = new XYSeries("sin(a)")

    var ii:Float = 0
    while(ii <  Math.PI){
      series.add(ii, Math.sin(ii))
      ii +=0.1f
    }

    var series2:XYSeries = new XYSeries("Hk/k от k")
  //  for(i <- arrSize.indices)
      for(j<- 0 until 5){
        series2.add(j+1,Hk(j))
      }
    var xyDataset2:XYDataset = new XYSeriesCollection(series2)
    var chart2:JFreeChart = ChartFactory.createXYLineChart("y = Hk/k", "k", "y", xyDataset2, PlotOrientation.VERTICAL, true, true, true)
    var frame2:JFrame = new JFrame("Зависимость Hk/k от к")
    // Помещаем график на фрейм
    frame2.getContentPane.add(new ChartPanel(chart2))
    frame2.setSize(600, 600)
    frame2.show()

  }
  def analiz(filename:String): Unit ={
    val source = Source.fromFile(filename, "UTF-8")
    val Ss:String = source.mkString
    var T: String = ""
    for(s <- Ss){
      if(!".,:;-?\t\n!/ =+".contains(s)){
        T+=s
      }
    }
    T = T.toLowerCase()
    println("T = " + T)
    val arrSize:ArrayBuffer[ArrayBuffer[Int]] = new ArrayBuffer[ArrayBuffer[Int]]

    val arrGram:ArrayBuffer[ArrayBuffer[String]] = new ArrayBuffer[ArrayBuffer[String]]
    var px:ArrayBuffer[ArrayBuffer[Double]] = new  ArrayBuffer[ArrayBuffer[Double]]
    var n: Array[Int] = new  Array[Int](5)

    for(i <- 0 to 4){
       arrGram += new ArrayBuffer[String]
       arrSize += new ArrayBuffer[Int]
       px += new ArrayBuffer[Double]
     }
    var ii = 0
    var str = ""
    for(i <- 0 to 4){
      for(j <- T.indices) {
        if( j + i + 1 <= T.length) {
          n(i) +=1
          str = T.substring(j, j + i + 1)
          if(j == 0){

            arrGram(i) += str
            ii += 1
          }
          else{
            if (arrGram(i).indexOf(str) == -1) {
              arrGram(i) += str
              ii += 1
            }
          }

        }
      }
      ii = 0
      for(k <- arrGram(i).indices) {
        println("ArrGram"+i+" "+k+" = " +arrGram(i)(k))
      }
    }

    for(i <- 0 to 4){
      for(j <- arrGram(i).indices) {
        val p: Pattern = Pattern.compile(arrGram(i)(j))
        val m: Matcher = p.matcher(T)
        var counter:Double = 0.0
        while(m.find()) {
          counter+=1
        }
        arrSize(i) += counter.toByte
        val pp:Double =  counter/n(i)
        px(i) += pp
        println("sizeArr" + i + " " + j+" = " + counter )
        println("px" + i + " " + j+" = " +  pp )
      }
    }


    var S:Double = 0.0
    for (i <- 0 to 4){
      for(j <- px(i).indices)
        S += px(i)(j)*log2(px(i)(j))
      Hk(i) = S/(i+1)*(-1)
      println("Hk"+i+1+"=" + Hk(i))
      S = 0
    }
    println("Hk" +  "="+ Hk(0)+Hk(1)+Hk(2)+Hk(3)+Hk(4))

  }
  def main(args:Array[String]) :Unit ={


    val filenameE =    "/home/nk16/Dropbox/1 семестр/криптографические методы защиты информации/LABS/lab7(sbt,scala)/src/main/resources/f.txt"
    analiz(filenameE)
    graf()

  }
}
